<?php

/**
 * visma/composer-patches-plugin, allows patching third party modules
 *
 * Copyright (C) 2019  Visma Digital Commerce AS
 * Copyright (C) 2014  Netresearch GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace Visma\ComposerPatches;

use Composer\Package\PackageInterface;
use Composer\Util\RemoteFilesystem;
use Exception;

/**
 * Represents a patch for a specific package in a specific version
 */
class Patch
{
    /**
     * @var resource|null
     */
    protected $contents;

    /**
     * @var string|null
     */
    protected ?string $title;

    /**
     * @var string|null
     */
    protected ?string $hash;

    /**
     * @var PackageInterface
     */
    private PackageInterface $containingPackage;

    /**
     * Patch constructor.
     * @param RemoteFilesystem $remoteFilesystem
     * @param string $package
     * @param string $url
     * @param string $version
     */
    public function __construct(
        private readonly RemoteFilesystem $remoteFilesystem,
        protected string $package,
        protected string $url,
        protected string $version = "*",
    ) {
    }

    /**
     * Returns url
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * Sets url
     *
     * @param string $url
     * @return Patch
     */
    public function setUrl(string $url): Patch
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Returns title
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Sets title
     *
     * @param string|null $title
     * @return Patch
     */
    public function setTitle(?string $title): Patch
    {
        $this->title = is_string($title) ? $title : null;
        return $this;
    }

    /**
     * Returns hash
     *
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * Sets hash
     *
     * @param string|null $hash
     * @return Patch
     */
    public function setHash(?string $hash): Patch
    {
        $this->hash = is_string($hash) ? $hash : null;
        return $this;
    }

    /**
     * Returns package
     *
     * @return string
     */
    public function getPackage(): string
    {
        return $this->package;
    }

    /**
     * Sets Package
     *
     * @param string $package
     * @return Patch
     */
    public function setPackage(string $package): Patch
    {
        $this->package = $package;
        return $this;
    }

    /**
     * Returns version
     *
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * Sets version
     *
     * @param string $version
     * @return Patch
     */
    public function setVersion(string $version): Patch
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get the patch contents
     *
     * @return resource
     * @throws Exception
     */
    public function getContents()
    {
        if ($this->contents === null) {
            $this->contents = $this->downloadContents($this->getUrl());
        }

        rewind($this->contents);
        return $this->contents;
    }

    /**
     * Returns containing patches
     *
     * @return PackageInterface
     */
    protected function getContainingPackage(): PackageInterface
    {
        return $this->containingPackage;
    }

    /**
     * Get the contents of the file from the remote
     *
     * @param string $url
     * @return resource
     * @throws Exception
     */
    private function downloadContents(string $url)
    {
        $originUrl = $this->getOriginUrl($url);
        /** @psalm-suppress InternalMethod */
        $contents = $this->remoteFilesystem->getContents(
            $originUrl ?: $url,
            $url ?: ' ',
            false
        );

        if (!is_string($contents)) {
            //phpcs:ignore Magento2.Exceptions.DirectThrow.FoundDirectThrow
            throw new Exception("Could not get file");
        }
        //phpcs:ignore Magento2.Functions.DiscouragedFunction.DiscouragedWithAlternative
        $stream = fopen("php://temp", "rw");

        if (!is_resource($stream)) {
            //phpcs:ignore Magento2.Exceptions.DirectThrow.FoundDirectThrow
            throw new Exception("Could not store patch in memory");
        }

        //phpcs:ignore Magento2.Functions.DiscouragedFunction.DiscouragedWithAlternative
        fwrite($stream, $contents);
        rewind($stream);
        return $stream;
    }

    /**
     * Sets containing package
     *
     * @param PackageInterface $package
     * @return Patch
     */
    public function setContainingPackage(PackageInterface $package): Patch
    {
        $this->containingPackage = $package;
        return $this;
    }

    /**
     * Returns original url
     *
     * @param string $url
     * @return string
     */
    private function getOriginUrl(string $url): string
    {
        //phpcs:ignore Magento2.Functions.DiscouragedFunction.Discouraged
        return (string)parse_url($url, PHP_URL_HOST);
    }
}
