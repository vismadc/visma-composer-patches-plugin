<?php

/**
 * visma/composer-patches-plugin, allows patching third party modules
 *
 * Copyright (C) 2019  Visma Digital Commerce AS
 * Copyright (C) 2014  Netresearch GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * @noinspection PhpUnusedParameterInspection
 */

declare(strict_types=1);

namespace Visma\ComposerPatches;

use Composer\Config;
use Composer\Util\HttpDownloader;
use Exception;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;
use Composer\Composer;
use Composer\EventDispatcher\Event as ComposerEvent;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Installer\PackageEvents;
use Composer\IO\IOInterface;
use Composer\Package\PackageInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\ScriptEvents;
use Composer\Util\RemoteFilesystem;
use Visma\ComposerPatches\Exception\PatchApplicationException;
use Visma\ComposerPatches\Util\Command;

/**
 * Main plugin entry point
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Patcher implements PluginInterface, EventSubscriberInterface
{
    public const PLUGIN_PRIORITY = 5;

    /**
     * @var Composer
     */
    private Composer $composer;

    /**
     * @var IOInterface
     */
    private IOInterface $composerIO;

    /**
     * @var Psr16Cache
     */
    private Psr16Cache $cache;

    /**
     * @inheritdoc
     *
     * phpcs:disable Magento2.Functions.StaticFunction
     */
    public static function getSubscribedEvents(): array
    {
        return [
            PackageEvents::PRE_PACKAGE_UNINSTALL => ['preRemove', self::PLUGIN_PRIORITY],
            PackageEvents::PRE_PACKAGE_UPDATE => ['preUpdate', self::PLUGIN_PRIORITY],
            ScriptEvents::POST_UPDATE_CMD => ['postUpdate', self::PLUGIN_PRIORITY],
            ScriptEvents::POST_INSTALL_CMD => ['postInstall', self::PLUGIN_PRIORITY],
        ];
    }

    /**
     * Apply plugin modifications to Composer
     *
     * @param Composer $composer
     * @param IOInterface $io
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function activate(Composer $composer, IOInterface $io): void
    {
        $this->resetProperties($composer, $io);
    }

    /**
     * Post install hook
     *
     * @param ComposerEvent $installEvent
     * @return void
     * @throws PatchApplicationException
     * @suppress PhanUnusedPublicNoOverrideMethodParameter
     */
    public function postInstall(ComposerEvent $installEvent): void
    {
        $this->installPatches($installEvent);
    }

    /**
     * Installs patches
     *
     * @param ComposerEvent $installEvent
     * @return void
     * @throws PatchApplicationException
     */
    protected function installPatches(ComposerEvent $installEvent): void
    {
        $this->writeMaintainingMessage($installEvent);

        $this->applyPatches(false);
    }

    /**
     * Resolves patches
     *
     * @return PatchList
     */
    protected function resolvePatches(): PatchList
    {
        $this->composerIO->write("<info>Resolving patches</info>", true, IOInterface::VERBOSE);

        $packages = $this->getPackagesWithPatches();

        $downloader = new HttpDownloader($this->composerIO, new Config());
        /** @psalm-suppress InternalMethod, InternalClass */
        $remoteFilesystem = new RemoteFilesystem($this->composerIO, new Config());
        $patchList = new PatchList($remoteFilesystem, $this->composerIO, $this->cache, null, $downloader);
        $patchList->addPackages($packages);

        return $patchList;
    }

    /**
     * Returns packages with patches
     *
     * @return PackageInterface[]
     */
    protected function getPackagesWithPatches(): array
    {
        $packages = $this->getPackages();
        return array_filter($packages, function (PackageInterface $package): bool {
            $extra = $package->getExtra();
            return array_key_exists("patches", $extra);
        });
    }

    /**
     * Returns packages
     *
     * @return PackageInterface[]
     */
    protected function getPackages(): array
    {
        return $this->composer->getRepositoryManager()
            ->getLocalRepository()
            ->getCanonicalPackages();
    }

    /**
     * Pre update hook
     *
     * @param ComposerEvent $updateEvent
     * @throws PatchApplicationException
     * @suppress PhanUnusedPublicNoOverrideMethodParameter
     */
    public function preUpdate(ComposerEvent $updateEvent): void
    {
        $this->uninstallPatches($updateEvent);
    }

    /**
     * Uninstalls patches
     *
     * @param ComposerEvent $event
     * @return void
     * @throws PatchApplicationException
     */
    protected function uninstallPatches(ComposerEvent $event): void
    {
        $this->writeMaintainingMessage($event);

        $this->applyPatches(true);
    }

    /**
     * Pre remove hook
     *
     * @param ComposerEvent $updateEvent
     * @return void
     * @throws PatchApplicationException
     * @suppress PhanUnusedPublicNoOverrideMethodParameter
     */
    public function preRemove(ComposerEvent $updateEvent): void
    {
        $this->uninstallPatches($updateEvent);
    }

    /**
     * Post update hook
     *
     * @param ComposerEvent $updateEvent
     * @return void
     * @throws PatchApplicationException
     * @suppress PhanUnusedPublicNoOverrideMethodParameter
     */
    public function postUpdate(ComposerEvent $updateEvent): void
    {
        $this->installPatches($updateEvent);
    }

    /**
     * Returns plain event name
     *
     * @param ComposerEvent $installEvent
     * @return string|null
     */
    private function getPlainEventName(ComposerEvent $installEvent): ?string
    {
        return match ($installEvent->getName()) {
            PackageEvents::PRE_PACKAGE_UNINSTALL => "package pre-uninstall",
            PackageEvents::PRE_PACKAGE_UPDATE => "package pre-update",
            ScriptEvents::POST_UPDATE_CMD => "post-update",
            ScriptEvents::POST_INSTALL_CMD => "post-install",
            default => null,
        };
    }

    /**
     * Prints maintaining message
     *
     * @param ComposerEvent $installEvent
     * @return void
     */
    private function writeMaintainingMessage(ComposerEvent $installEvent): void
    {
        $event = $this->getPlainEventName($installEvent);
        $message = "<info>Maintaining patches</info>";
        if ($event !== null) {
            $message .= " after <comment>{$event}</comment>";
        }

        $this->composerIO->write($message);
    }

    /**
     * Deactivates patches
     *
     * @param Composer $composer
     * @param IOInterface $io
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function deactivate(Composer $composer, IOInterface $io): void
    {
        $this->resetProperties($composer, $io);

        try {
            $this->applyPatches(true);
        } catch (Exception $e) {
            $this->composerIO->write("<error>Reverting patches on 'deactivate' failed</error>");
            $this->composerIO->write("{$e->getMessage()}", true);
        }
    }

    /**
     * Uninstalls patches
     *
     * @param Composer $composer
     * @param IOInterface $io
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function uninstall(Composer $composer, IOInterface $io): void
    {
        $this->resetProperties($composer, $io);

        try {
            $this->applyPatches(true);
        } catch (Exception $e) {
            $this->composerIO->write("<error>Reverting patches on 'uninstall' failed</error>");
            $this->composerIO->write("{$e->getMessage()}", true);
        }
    }

    /**
     * Resets properties
     *
     * @param Composer $composer
     * @param IOInterface $composerIO
     */
    private function resetProperties(Composer $composer, IOInterface $composerIO): void
    {
        $this->composer = $composer;
        $this->composerIO = $composerIO;
        $this->cache = new Psr16Cache(new ArrayAdapter());
    }

    /**
     * Applies patches
     *
     * @param bool $reverse
     * @throws PatchApplicationException|PatchApplicationException
     */
    private function applyPatches(bool $reverse): void
    {
        $patchList = $this->resolvePatches();

        $patcher = new Util\Patcher($this->composer, new Command(), $this->composerIO);

        $patcher->applyPatches($patchList, $this->getPackages(), $reverse);
    }
}
