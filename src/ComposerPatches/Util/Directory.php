<?php

declare(strict_types=1);

namespace Visma\ComposerPatches\Util;

use Exception;

/**
 * phpcs:disable Magento2.PHP.FinalImplementation.FoundFinal
 * phpcs:disable Magento2.Functions.StaticFunction.StaticFunction
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
final class Directory
{
    /**
     * Recursively delete a directory and its contents
     *
     * @param string $dir
     * @return void
     * @see https://stackoverflow.com/a/3338133
     * phpcs:disable Magento2.Functions.DiscouragedFunction.DiscouragedWithAlternative
     * phpcs:disable Magento2.Functions.DiscouragedFunction.Discouraged
     * phpcs:disable Squiz.Functions.GlobalFunction.Found
     */
    public static function rrmdir(string $dir): void
    {
        if (!is_dir($dir)) {
            return;
        }

        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object == "." || $object == "..") {
                continue;
            }
            $path = implode(DIRECTORY_SEPARATOR, [$dir, $object]);
            if (is_dir($path)) {
                Directory::rrmdir($path);
                continue;
            }

            unlink($path);
        }

        rmdir($dir);
    }

    /**
     * Recursively copy a directory and its contents
     *
     * @param string $src
     * @param string $dst
     *
     * @see https://stackoverflow.com/questions/2050859/copy-entire-contents-of-a-directory-to-another-using-php
     * @suppress PhanPossiblyFalseTypeArgumentInternal
     * phpcs:disable Magento2.Functions.DiscouragedFunction.DiscouragedWithAlternative
     * phpcs:disable Magento2.Functions.DiscouragedFunction.Discouraged
     * phpcs:disable Squiz.Functions.GlobalFunction.Found
     */
    public static function rcopy(string $src, string $dst): void
    {
        $dir = opendir($src);
        mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file == '.') || ($file == '..')) {
                continue;
            }
            $srcPath = $src . DIRECTORY_SEPARATOR . $file;
            $dstPath = $dst . DIRECTORY_SEPARATOR . $file;
            if (is_dir($srcPath)) {
                Directory::rcopy($srcPath, $dstPath);
                continue;
            }

            copy($srcPath, $dstPath);
        }
        closedir($dir);
    }

    /**
     * Creates a random unique temporary directory, with specified parameters,
     *
     * That does not already exist (like tempnam(), but for dirs).
     *
     * Created dir will begin with the specified prefix, followed by random
     * numbers.
     *
     * @link https://php.net/manual/en/function.tempnam.php
     *
     * @param string|null $dir Base directory under which to create temp dir.
     *     If null, the default system temp dir (sys_get_temp_dir()) will be
     *     used.
     * @param string $prefix String with which to prefix created dirs.
     * @param int $mode Octal file permission mask for the newly-created dir.
     *     Should begin with a 0.
     * @param int $maxAttempts Maximum attempts before giving up (to prevent
     *     endless loops).
     * @return string|bool Full path to newly-created dir, or false on failure.
     *
     * @see https://stackoverflow.com/a/30010928
     * phpcs:disable Magento2.Functions.DiscouragedFunction.DiscouragedWithAlternative
     * phpcs:disable Squiz.Functions.GlobalFunction.Found
     * @throws Exception
     */
    public static function tempdir($dir = null, $prefix = 'tmp_', $mode = 0700, $maxAttempts = 1000)
    {
        /* Use the system temp dir by default. */
        if ($dir === null) {
            $dir = sys_get_temp_dir();
        }

        /* Trim trailing slashes from $dir. */
        $dir = rtrim($dir, DIRECTORY_SEPARATOR);

        /* If we don't have permission to create a directory, fail, otherwise we will
         * be stuck in an endless loop.
         */
        if (!is_dir($dir) || !is_writable($dir)) {
            return false;
        }

        /* Make sure characters in prefix are safe. */
        if (strpbrk($prefix, '\\/:*?"<>|') !== false) {
            return false;
        }

        /* Attempt to create a random directory until it works. Abort if we reach
         * $maxAttempts. Something screwy could be happening with the filesystem
         * and our loop could otherwise become endless.
         */
        $attempts = 0;
        do {
            $path = sprintf('%s%s%s%d', $dir, DIRECTORY_SEPARATOR, $prefix, random_int(100000, mt_getrandmax()));
        } while (!mkdir($path, $mode) && $attempts++ < $maxAttempts);

        return $path;
    }
}
