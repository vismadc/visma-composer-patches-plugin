<?php

/**
 * visma/composer-patches-plugin, allows patching third party modules
 *
 * Copyright (C) 2019  Visma Digital Commerce AS
 * Copyright (C) 2014  Netresearch GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace Visma\ComposerPatches\Util;

use Exception;
use Throwable;

/**
 * Command and execution helper
 * phpcs:disable Magento2.Functions.DiscouragedFunction.Discouraged
 * phpcs:disable Magento2.Security.InsecureFunction.Found
 * phpcs:disable Magento2.Functions.DiscouragedFunction.DiscouragedWithAlternative
 */
class Command
{
    /**
     * Process execution wrapper adapted from
     *
     * @see http://omegadelta.net/2012/02/08/stdin-stdout-stderr-with-proc_open-in-php/
     *
     * @see https://git.io/fjWsk
     *
     * @param string $command
     * @param string $cwd
     * @param string $stdin
     * @param string $stdout
     * @param string $stderr
     * @return int
     * @throws Exception
     */
    public function executeProcess(
        string $command,
        string $cwd,
        string $stdin,
        string &$stdout,
        string &$stderr
    ): int {
        $descriptorSpec = [
            0 => ['pipe', 'r'],
            1 => ['pipe', 'w'],
            2 => ['pipe', 'w']
        ];
        $process = proc_open($command, $descriptorSpec, $pipes, $cwd);
        $txOff = 0;
        $txLen = strlen($stdin);

        $stdout = '';
        $stderr = '';

        $statusCode = $this->makeStdNonBlocking($pipes);
        if ($statusCode != 0) {
            return proc_close($process);
        }

        if ($txLen == 0) {
            fclose($pipes[0]);
        }

        return $this->loopProcess($pipes, $txOff, $txLen, $process, $stdin, $stdout, $stderr);
    }

    /**
     * Make stdin/stdout/stderr non-blocking
     *
     * @param array $pipes
     * @return int
     */
    private function makeStdNonBlocking(array $pipes): int
    {
        try {
            stream_set_blocking($pipes[0], false);
            stream_set_blocking($pipes[1], false);
            stream_set_blocking($pipes[2], false);

            return 0;
        } catch (Throwable) {
            return 1;
        }
    }

    /**
     * Reads pipes
     *
     * @param mixed $stdoutDone
     * @param array $pipes
     * @param mixed $stderrDone
     * @return array
     */
    protected function readPipes(mixed $stdoutDone, array $pipes, mixed $stderrDone): array
    {
        $read = [];
        if (!$stdoutDone) {
            $read[] = $pipes[1];
        }
        if (!$stderrDone) {
            $read[] = $pipes[2];
        }

        return $read;
    }

    /**
     * Writes pipes
     *
     * @param string $stdin
     * @param int $txOff
     * @param resource $pipe
     * @param int $txLen
     * @return int
     */
    protected function writePipes(string $stdin, int $txOff, $pipe, int $txLen): int
    {
        $str = substr($stdin, $txOff, 8192);
        $txRet = fwrite($pipe, $str);
        if ($txRet !== false) {
            $txOff += $txRet;
        }
        if ($txOff >= $txLen) {
            fclose($pipe);
        }
        return $txOff;
    }

    /**
     * Process read pipes
     *
     * @param array $read
     * @param array $pipes
     * @param string $stdout
     * @param bool $stdoutDone
     * @param string $stderr
     * @param bool $stderrDone
     * @return array
     */
    protected function processReadPipes(
        array $read,
        array $pipes,
        string $stdout,
        bool $stdoutDone,
        string $stderr,
        bool $stderrDone
    ): array {
        foreach ($read as $r) {
            if ($r == $pipes[1]) {
                $stdout .= fread($pipes[1], 8192);
                if (feof($pipes[1])) {
                    fclose($pipes[1]);
                    $stdoutDone = true;
                }
            } elseif ($r == $pipes[2]) {
                $stderr .= fread($pipes[2], 8192);
                if (feof($pipes[2])) {
                    fclose($pipes[2]);
                    $stderrDone = true;
                }
            }
        }

        return [$stdout, $stdoutDone, $stderr, $stderrDone];
    }

    /**
     * Loops through pipes
     *
     * @param array $pipes
     * @param int $txOff
     * @param int $txLen
     * @param resource $process
     * @param string $stdin
     * @param mixed $stdout
     * @param mixed $stderr
     * @return int
     */
    protected function loopProcess(
        array $pipes,
        int $txOff,
        int $txLen,
        $process,
        string $stdin,
        string $stdout,
        string $stderr
    ): int {
        $stdoutDone = false;
        $stderrDone = false;

        while (true) {
            // The program's stdout/stderr
            $read = $this->readPipes($stdoutDone, $pipes, $stderrDone);
            // The program's stdin
            $write = [];
            if ($txOff < $txLen) {
                $write[] = $pipes[0];
            }
            $except = null;
            // Block til r/w possible
            try {
                stream_select($read, $write, $except, 0, 0);
            } catch (Throwable $exception) {
                return proc_close($process);
            }
            if (!empty($write)) {
                $txOff = $this->writePipes($stdin, $txOff, $pipes[0], $txLen);
            }
            [$stdout, $stdoutDone, $stderr, $stderrDone] = $this->processReadPipes(
                $read ?? [],
                $pipes,
                $stdout,
                $stdoutDone,
                $stderr,
                $stderrDone
            );

            if (!is_resource($process)) {
                break;
            }
            if ($txOff >= $txLen && $stdoutDone && $stderrDone) {
                break;
            }
        }

        return proc_close($process);
    }
}
