<?php

/**
 * visma/composer-patches-plugin, allows patching third party modules
 *
 * Copyright (C) 2019  Visma Digital Commerce AS
 * Copyright (C) 2014  Netresearch GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace Visma\ComposerPatches\Util;

use Composer\Composer;
use Composer\IO\IOInterface as IO;
use Composer\Package\PackageInterface;
use Composer\Semver\Semver;
use Exception;
use Throwable;
use Visma\ComposerPatches\Exception\PatchApplicationException;
use Visma\ComposerPatches\Patch;
use Visma\ComposerPatches\PatchList;

/**
 * Utility class to apply patches
 */
class Patcher
{
    /**
     * Constructor
     *
     * @param Composer $composer
     * @param Command $command
     * @param IO $io
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function __construct(
        private readonly Composer $composer,
        private readonly Command $command,
        private readonly IO $io
    ) {
    }

    /**
     * Applies patchets
     *
     * @param PatchList $patchList
     * @param PackageInterface[] $packages
     * @param bool $reverse Uninstall
     * @throws PatchApplicationException
     * @suppress PhanUnusedVariableCaughtException
     */
    public function applyPatches(PatchList $patchList, array $packages, bool $reverse): void
    {
        foreach ($packages as $package) {
            $this->io->write(
                "Evaluating patches for <comment>{$package}</comment>",
                true,
                IO::VERY_VERBOSE
            );
            $patches = $this->getValidPatches($patchList, $package);
            foreach ($patches as $patch) {
                try {
                    $this->applyPatch($patch, $package, true, $reverse);

                    $this->io->write(sprintf(
                        "- %s patch to <info>%s</info> (<comment>%s</comment>): %s",
                        $reverse ? "Removing" : "Applying",
                        $package->getName(),
                        $patch->getVersion(),
                        $patch->getTitle() ?? "No description"
                    ));

                    $this->applyPatch($patch, $package, false, $reverse);
                } catch (PatchApplicationException $applicationException) {
                    $this->applyPatch($patch, $package, true, !$reverse);

                    $this->io->write("Patch for {$package->getName()} is already applied", true, IO::VERBOSE);
                }
            }
        }
    }

    /**
     * Returns valid patches
     *
     * @param PatchList $patchList
     * @param PackageInterface $package
     * @return Patch[]
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    protected function getValidPatches(PatchList $patchList, PackageInterface $package): array
    {
        // Filter by name
        $patches = array_filter($patchList->getPatches(), function (Patch $patch) use ($package) {
            return strcasecmp($package->getName(), $patch->getPackage()) === 0;
        });

        // Filter by constraint
        return array_filter($patches, function (Patch $patch) use ($package) {
            return Semver::satisfies($package->getVersion(), $patch->getVersion());
        });
    }

    /**
     * Applies patch
     *
     * @param Patch $patch
     * @param PackageInterface $package
     * @param bool $dryRun
     * @param bool $revert
     * @return bool
     * @throws PatchApplicationException
     */
    public function applyPatch(Patch $patch, PackageInterface $package, bool $dryRun, bool $revert)
    {
        $path = $this->getPackagePath($package);
        $contents = $patch->getContents();

        $command = $this->whichPatchCmd() . ' --force --strip=1 --no-backup-if-mismatch -r -';

        if ($dryRun) {
            $command .= ' --dry-run';
        }

        if ($revert) {
            $command .= ' --reverse';
        }

        rewind($contents);
        try {
            //phpcs:ignore Magento2.Functions.DiscouragedFunction.Discouraged
            $contentsAsString = stream_get_contents($contents);
        } catch (Throwable $exception) {
            return false;
        }

        if (is_bool($contentsAsString)) {
            throw new PatchApplicationException("Could not open stream with patch contents");
        }

        $output = '';
        $errors = '';
        $return = $this->command->executeProcess($command, $path, $contentsAsString, $output, $errors);
        if ($return > 0) {
            throw new PatchApplicationException(
                "Unable to apply patch"
                . PHP_EOL . $output
                . PHP_EOL . $errors
            );
        }

        return true;
    }

    /**
     * Locate the patch executable
     *
     * @return string|null
     * @throws Exception
     */
    protected function whichPatchCmd(): ?string
    {
        static $patchCommand = null;
        if (!$patchCommand) {
            $exitCode = $output = null;
            //phpcs:ignore Magento2.Security.InsecureFunction.FoundWithAlternative
            $patchCommand = (string)exec('which patch', $output, $exitCode);
            //phpcs:ignore Magento2.Functions.DiscouragedFunction.Discouraged
            if (0 !== $exitCode || !is_executable($patchCommand)) {
                throw new Exception(
                    "Cannot find the 'patch' executable command."
                );
            }
        }

        return $patchCommand;
    }

    /**
     * Get the filesystem location for the specific package
     *
     * @param PackageInterface $package
     * @return string
     */
    protected function getPackagePath(PackageInterface $package): string
    {
        return (string)$this->composer->getInstallationManager()->getInstallPath($package);
    }
}
