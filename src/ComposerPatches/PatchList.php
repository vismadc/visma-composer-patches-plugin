<?php

/**
 * visma/composer-patches-plugin, allows patching third party modules
 *
 * Copyright (C) 2019  Visma Digital Commerce AS
 * Copyright (C) 2014  Netresearch GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace Visma\ComposerPatches;

use Composer\IO\IOInterface;
use Composer\Json\JsonFile;
use Composer\Package\PackageInterface;
use Composer\Util\HttpDownloader;
use Composer\Util\RemoteFilesystem;
use Psr\SimpleCache\CacheInterface;
use Visma\ComposerPatches\Exception\InvalidPatchDataException;

class PatchList
{
    /**
     * @var Patch[]
     */
    protected array $patches;

    /**
     * PatchList constructor.
     * @param RemoteFilesystem $remoteFilesystem
     * @param IOInterface $composerIO
     * @param CacheInterface $cache
     * @param JsonFile|null $jsonFile
     * @param HttpDownloader|null $httpDownloader
     */
    public function __construct(
        private readonly RemoteFilesystem $remoteFilesystem,
        private readonly IOInterface $composerIO,
        private readonly CacheInterface $cache,
        private readonly ?JsonFile $jsonFile = null,
        private readonly ?HttpDownloader $httpDownloader = null,
    ) {
        $this->patches = [];
    }

    /**
     * Add packages and extract the patches from them
     *
     * @param PackageInterface[] $packages
     */
    public function addPackages(array $packages): PatchList
    {
        foreach ($packages as $package) {
            foreach ($this->extractPatches($package) as $patch) {
                $this->addPatch($patch);
            }
        }

        return $this;
    }

    /**
     * Extracts packages
     *
     * @param PackageInterface $package
     * @return Patch[]
     */
    protected function extractPatches(PackageInterface $package): array
    {
        $extra = $package->getExtra();

        // Does not contain patches
        if (!array_key_exists("patches", $extra)) {
            return [];
        }

        $packages = $extra["patches"];

        if (is_string($packages)) {
            $packages = $this->getRemoteJson($packages);
        }

        // Turns the data into a valid patch array
        $packages = $this->innerGetPackages($packages);

        $patches = $this->recursiveFindPatches($packages);

        foreach ($patches as $patch) {
            /** @var Patch $patch */
            $patch->setContainingPackage($package);
        }

        return $patches;
    }

    /**
     * Returns inner packages
     *
     * @param array $data
     * @return array
     * @throws InvalidPatchDataException
     */
    protected function innerGetPackages(array $data): array
    {
        // Loop through the patches and make sure we have a map of version
        // constraints, or alternatively just an array of patches
        return array_map(function ($constraints, $package) {
            if (is_string($constraints)) {
                $constraints = $this->getRemoteJson($constraints);
            }
            if (!is_array($constraints)) {
                throw new InvalidPatchDataException("Patch contains invalid data");
            }

            return $this->innerGetConstraints($constraints, (string)$package);
        }, $data, array_keys($data));
    }

    /**
     * Returns remote JSON
     *
     * @param string $remote
     * @return mixed
     */
    protected function getRemoteJson(string $remote)
    {
        if ($this->cache->has(hash("sha256", $remote))) {
            $this->composerIO->write("Read {$remote} from cache", true, IOInterface::VERY_VERBOSE);
            return $this->cache->get(hash("sha256", $remote));
        }

        $json = $this->getJsonFile($remote);

        $data = $json->read();
        $this->cache->set(hash("sha256", $remote), $data);

        return $data;
    }

    /**
     * Adds a patch
     *
     * @param Patch $patch
     * @return PatchList
     */
    protected function addPatch(Patch $patch): PatchList
    {
        $this->patches[] = $patch;

        return $this;
    }

    /**
     * Returns patches
     *
     * @return Patch[]
     */
    public function getPatches(): array
    {
        return $this->patches;
    }

    /**
     * Finds patches recursively
     *
     * @param mixed $data
     * @return array
     */
    protected function recursiveFindPatches($data): array
    {
        $patches = [];

        if ($data instanceof Patch) {
            $patches[] = $data;
        }

        if (is_array($data)) {
            $patches = array_reduce($data, function ($carry, $item) {
                $item = $this->recursiveFindPatches($item);
                return array_merge($carry, $item);
            }, $patches);
        }

        return $patches;
    }

    /**
     * [constraint => children, constraint => children]
     */

    /**
     * Returns inner constraints
     *
     * @param array $data
     * @param string $package
     * @return array
     */
    protected function innerGetConstraints(array $data, string $package): array
    {
        return array_map(function ($patches, $constraint) use ($package) {
            if (is_string($patches)) {
                $patches = $this->getRemoteJson($patches);
            }
            if (!is_array($patches)) {
                throw new InvalidPatchDataException("Patch contains invalid data");
            }

            if (array_key_exists("url", $patches)) {
                return $this->innerGetPatch($patches, "*", $package);
            }

            return $this->innerGetPatches($patches, $constraint, $package);
        }, $data, array_keys($data));
    }

    /**
     * Returns inner pathces
     *
     * @param array $data
     * @param string $constraint
     * @param string $package
     * @return array
     */
    private function innerGetPatches(array $data, string $constraint, string $package): array
    {
        return array_map(function ($patch) use ($constraint, $package) {
            if (is_string($patch)) {
                $patch = $this->getRemoteJson($patch);
            }
            if (!is_array($patch)) {
                throw new InvalidPatchDataException("Patch contains invalid data");
            }

            return $this->innerGetPatch($patch, $constraint, $package);
        }, $data);
    }

    /**
     * Returns inner path
     *
     * @param array $data
     * @param string $constraint
     * @param string $package
     * @return Patch
     */
    private function innerGetPatch(array $data, string $constraint, string $package): Patch
    {
        $patch = new Patch($this->remoteFilesystem, $package, $data["url"], $constraint);

        if (array_key_exists("sha1", $data)) {
            $patch->setHash($data["sha1"]);
        }

        if (array_key_exists("title", $data)) {
            $patch->setTitle($data["title"]);
        }

        return $patch;
    }

    /**
     * Returns JSON file
     *
     * @param string $path
     * @return JsonFile
     */
    protected function getJsonFile(string $path): JsonFile
    {
        return ($this->jsonFile === null)
            ? new JsonFile($path, $this->httpDownloader, $this->composerIO)
            : $this->jsonFile;
    }
}
