const AccessSniff = require('access-sniff');
const fs = require('fs');

const options = JSON.parse(fs.readFileSync('.a11y.json').toString());
const reportOptions = {
    options: {
        reportType: 'json',
        reportLocation: 'reports',
        reportLevels: {
            notice: true,
            warning: true,
            error: true
        }
    }
}

AccessSniff
    .default(['src/**/*.*html'], options)
    .then(function(report) {
        AccessSniff.report(report, reportOptions);
    })
    .catch(function (error) {
        console.log(error)
    });
