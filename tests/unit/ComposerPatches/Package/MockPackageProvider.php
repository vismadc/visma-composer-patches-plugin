<?php

/**
 * visma/composer-patches-plugin, allows patching third party modules
 *
 * Copyright (C) 2019  Visma Digital Commerce AS
 * Copyright (C) 2014  Netresearch GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace Visma\ComposerPatches\Test\Package;

use Composer\Package\Loader\ArrayLoader;
use Composer\Package\Loader\JsonLoader;

class MockPackageProvider
{
    /**
     * @param $filename
     * @return resource
     */
    protected function getFilename($filename)
    {
        return implode(DIRECTORY_SEPARATOR, [__DIR__, 'Fixtures', $filename]);
    }

    public function getMixedPatch()
    {
        $loader = new JsonLoader(new ArrayLoader());

        $filename = $this->getFilename("package-mixed-patch.json");
        $package = $loader->load($filename);

        return $package;
    }

    /**
     * @param string $filename
     * @return bool|resource
     * @throws \Exception
     */
    protected function getFileHandle(string $filename)
    {
        $handle = fopen($filename, "r");

        if (!is_resource($handle)) {
            throw new \Exception("File could not be opened");
        }

        return $handle;
    }

    /**
     * @return \Composer\Package\PackageInterface
     */
    public function getBasicPackage()
    {
        $loader = new JsonLoader(new ArrayLoader());

        $filename = $this->getFilename("package-basic.json");
        $package = $loader->load($filename);

        return $package;
    }


    /**
     * @return \Composer\Package\PackageInterface
     */
    public function getBasicPatch()
    {
        $loader = new JsonLoader(new ArrayLoader());

        $filename = $this->getFilename("package-basic-patch.json");
        $package = $loader->load($filename);

        return $package;
    }

    /**
     * @return \Composer\Package\PackageInterface
     */
    public function getExternalPatch()
    {
        $loader = new JsonLoader(new ArrayLoader());

        $filename = $this->getFilename("package-external-patch.json");
        $package = $loader->load($filename);

        return $package;
    }
}
