<?php

/**
 * visma/composer-patches-plugin, allows patching third party modules
 *
 * Copyright (C) 2019  Visma Digital Commerce AS
 * Copyright (C) 2014  Netresearch GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace Visma\ComposerPatches\Test;

use Composer\Config;
use Composer\IO\IOInterface;
use Composer\Package\PackageInterface;
use Composer\Util\RemoteFilesystem;
use Exception;
use PHPUnit\Framework\Constraint\IsType;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Visma\ComposerPatches\Patch;

class PatchTest extends TestCase
{
    /**
     * @return void
     * @throws Exception
     * @covers \Visma\ComposerPatches\Patch::getContents
     */
    public function testCanDownloadPatch()
    {
        /** @var RemoteFilesystem|MockObject $remoteFilesystemMock */
        $remoteFilesystemMock = $this->createMock(RemoteFilesystem::class);

        $patch = new Patch(
            $remoteFilesystemMock,
            "unit/test-package",
            "http://example.com/patchone.patch",
            "*"
        );
        $patch->setContainingPackage($this->createMock(PackageInterface::class));

        $patchContents = file_get_contents(implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, "Patch", "Fixtures", "patches", "not-a-patch.patch"]
        ));

        $remoteFilesystemMock->method("getContents")
            ->with(
                $this->logicalAnd(
                    $this->isType(IsType::TYPE_STRING),
                    $this->equalTo("example.com")
                ),
                $this->logicalAnd(
                    $this->isType(IsType::TYPE_STRING),
                    $this->equalTo("http://example.com/patchone.patch")
                )
            )
            ->willReturn($patchContents);

        $contents = $patch->getContents();
        rewind($contents);

        $this->assertEquals($patchContents, stream_get_contents($contents));
    }

    /**
     * @return void
     * @throws Exception
     * @covers \Visma\ComposerPatches\Patch::getContents
     */
    public function testCanUseLocalPatch()
    {
        /** @var RemoteFilesystem|MockObject $downloader */
        $downloader = new RemoteFilesystem(
            $this->createMock(IOInterface::class),
            $this->createMock(Config::class)
        );

        $patchPath = implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, "Patch", "Fixtures", "patches", "not-a-patch.patch"]
        );

        $patchContents = file_get_contents($patchPath);
        $patch = new Patch(
            $downloader,
            "unit/test-package",
            $patchPath,
            "*"
        );
        $patch->setContainingPackage($this->createMock(PackageInterface::class));

        $contents = $patch->getContents();
        rewind($contents);

        $this->assertEquals($patchContents, $patchContents);
    }
}
