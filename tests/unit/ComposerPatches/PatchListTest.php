<?php

/**
 * visma/composer-patches-plugin, allows patching third party modules
 *
 * Copyright (C) 2019  Visma Digital Commerce AS
 * Copyright (C) 2014  Netresearch GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace Visma\ComposerPatches\Test;

use Composer\Installer\InstallationManager;
use Composer\IO\IOInterface;
use Composer\Json\JsonFile;
use Composer\Util\HttpDownloader;
use Composer\Util\RemoteFilesystem;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;
use Visma\ComposerPatches\PatchList;
use Visma\ComposerPatches\Test\Package\MockPackageProvider;

class PatchListTest extends TestCase
{
    /**
     * @return void
     * @covers \Visma\ComposerPatches\PatchList::getPatches
     */
    public function testCanSetAndGetPatches()
    {
        /**
         * @var RemoteFilesystem|MockObject $remote
         * @var HttpDownloader|MockObject $httpDownloader
         * @var IOInterface|MockObject $io
         */
        $remote = $this->createMock(RemoteFilesystem::class);
        $io = $this->createMock(IOInterface::class);
        $im = $this->createMock(InstallationManager::class);
        $im->method("getInstallPath")->willReturn("/");

        $patchList = new PatchList($remote, $io, new Psr16Cache(new ArrayAdapter()));

        $packageProvider = new MockPackageProvider();
        $patch = $packageProvider->getBasicPatch();

        $patchList->addPackages([$patch]);

        $this->assertEquals(1, count($patchList->getPatches()));
    }

    /**
     * @return void
     * @covers \Visma\ComposerPatches\PatchList::getPatches
     */
    public function testCanGetMixedPatches()
    {
        /**
         * @var RemoteFilesystem|MockObject $remote
         * @var HttpDownloader|MockObject $httpDownloader
         * @var IOInterface|MockObject $io
         */
        $remote = $this->createMock(RemoteFilesystem::class);
        $io = $this->createMock(IOInterface::class);
        $packageProvider = new MockPackageProvider();
        $im = $this->createMock(InstallationManager::class);
        $im->method("getInstallPath")->willReturn("/");

        $patchList = new PatchList($remote, $io, new Psr16Cache(new ArrayAdapter()));
        $patch = $packageProvider->getMixedPatch();
        $patchList->addPackages([$patch]);

        $this->assertEquals(2, count($patchList->getPatches()));
    }

    /**
     * @return void
     * @covers \Visma\ComposerPatches\PatchList::getPatches
     */
    public function testCanGetExternalPatches()
    {
        /**
         * @var RemoteFilesystem|MockObject $remote
         * @var HttpDownloader|MockObject $httpDownloader
         * @var IOInterface|MockObject $io
         */
        $remote = $this->createMock(RemoteFilesystem::class);
        $httpDownloader = $this->createMock(HttpDownloader::class);
        $io = $this->createMock(IOInterface::class);
        $jsonFileMock = $this->createMock(JsonFile::class);

        $packageProvider = new MockPackageProvider();

        $jsonFileMock->method("read")->willReturn(
            json_decode(
                file_get_contents(
                    implode(
                        DIRECTORY_SEPARATOR,
                        [__DIR__, "Package", "Fixtures", "package-external-patch-external-content.json"]
                    )
                ),
                true
            )
        );

        $im = $this->createMock(InstallationManager::class);
        $im->method("getInstallPath")->willReturn("/");

        $patchList = new PatchList($remote, $io, new Psr16Cache(new ArrayAdapter()), $jsonFileMock, $httpDownloader);
        $patch = $packageProvider->getExternalPatch();
        $patchList->addPackages([$patch]);

        $patches = $patchList->getPatches();
        $first = array_values($patches)[0];

        $this->assertEquals(1, count($patches));
        $this->assertEquals("visma/package-basic", $first->getPackage());
        $this->assertEquals("Example remote patch", $first->getTitle());
    }
}
