<?php

/**
 * visma/composer-patches-plugin, allows patching third party modules
 *
 * Copyright (C) 2019  Visma Digital Commerce AS
 * Copyright (C) 2014  Netresearch GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace Visma\ComposerPatches\Test;

use Composer\Composer;
use Composer\Config;
use Composer\EventDispatcher\Event;
use Composer\Installer\InstallationManager;
use Composer\Installer\PackageEvents;
use Composer\IO\IOInterface;
use Composer\Package\Loader\ArrayLoader;
use Composer\Package\Loader\JsonLoader;
use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;
use Composer\Repository\RepositoryManager;
use Composer\Script\ScriptEvents;
use Composer\Util\RemoteFilesystem;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Psr16Cache;
use Visma\ComposerPatches\Exception\PatchApplicationException;
use Visma\ComposerPatches\Patch;
use Visma\ComposerPatches\Patcher;
use Visma\ComposerPatches\PatchList;
use Visma\ComposerPatches\Util\Command;
use Visma\ComposerPatches\Util\Directory;

class PatcherTest extends TestCase
{
    /**
     * @inheritdoc
     *
     * @return void
     * @throws Exception
     */
    protected function tearDown(): void
    {
        Directory::rrmdir(Directory::tempdir());
    }

    /**
     * Test that the creation and activation of the class works as expected
     *
     * @doesNotPerformAssertions
     * @covers \Visma\ComposerPatches\Patcher::activate
     */
    public function testCanBeCreated()
    {
        $patcher = new Patcher();

        $patcher->activate(
            $this->createMock(Composer::class),
            $this->createMock(IOInterface::class)
        );
    }

    /**
     * Test that the required events are returned
     * @covers \Visma\ComposerPatches\Patcher::getSubscribedEvents
     */
    public function testReturnsEventList()
    {
        $events = Patcher::getSubscribedEvents();

        $this->assertArrayHasKey(PackageEvents::PRE_PACKAGE_UNINSTALL, $events);
        $this->assertArrayHasKey(PackageEvents::PRE_PACKAGE_UPDATE, $events);
        $this->assertArrayHasKey(ScriptEvents::POST_UPDATE_CMD, $events);
        $this->assertArrayHasKey(ScriptEvents::POST_INSTALL_CMD, $events);

        $this->assertEquals($events[ScriptEvents::POST_UPDATE_CMD], ["postUpdate", Patcher::PLUGIN_PRIORITY]);
        $this->assertEquals($events[ScriptEvents::POST_INSTALL_CMD], ["postInstall", Patcher::PLUGIN_PRIORITY]);
    }

    /**
     * @doesNotPerformAssertions
     * @covers \Visma\ComposerPatches\Patcher::activate
     */
    public function testResolvesPatches()
    {
        $patcher = new Patcher();

        $repositoryManager = $this->createMock(RepositoryManager::class);
        $composer = $this->createMock(Composer::class);
        $composer->method("getRepositoryManager")->willReturn($repositoryManager);
        $im = $this->createMock(InstallationManager::class);
        $composer->method("getInstallationManager")->willReturn($im);
        $im->method("getInstallPath")->willReturn("/");
        $writableRepository = $this->createMock(InstalledRepositoryInterface::class);
        $repositoryManager->method("getLocalRepository")->willReturn($writableRepository);
        $writableRepository->method("getCanonicalPackages")->willReturn([]);

        $patcher->activate(
            $composer,
            $this->createMock(IOInterface::class)
        );

        /** @var Event|MockObject $event */
        $event = $this->createMock(Event::class);

        $patcher->postInstall(
            $event
        );
    }

    /**
     * Test for patch apply
     *
     * @return void
     * @throws PatchApplicationException
     * @covers \Visma\ComposerPatches\Util\Patcher::applyPatch
     */
    public function testAppliesPatch()
    {
        $dir = Directory::tempdir();

        $fileToPatch = implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, 'Patcher', 'Fixtures', 'files', 'broken-class-1.php']
        );

        $patchToApply = implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, 'Patcher', 'Fixtures', 'patches', 'patch-class-1.patch']
        );

        $expectedOutputFile = implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, 'Patcher', 'Fixtures', 'files', 'fixed-class-1.php']
        );
        $fileName = basename($fileToPatch);

        $tempFile = implode(DIRECTORY_SEPARATOR, [$dir, $fileName]);

        // Copy broken file in
        copy($fileToPatch, $tempFile);

        /**
         * @var Composer|MockObject $composer
         * @var InstallationManager|MockObject $installationManager
         * @var Patch|MockObject $patch
         * @var PackageInterface|MockObject $package
         */
        $composer = $this->createMock(Composer::class);
        $patch = $this->createMock(Patch::class);
        $package = $this->createMock(PackageInterface::class);
        $installationManager = $this->createMock(InstallationManager::class);
        $installationManager->method("getInstallPath")->willReturn($dir);
        $composer->method("getInstallationManager")->willReturn($installationManager);

        $patcher = new \Visma\ComposerPatches\Util\Patcher(
            $composer,
            new Command(),
            $this->createMock(IOInterface::class)
        );

        $patch->method("getContents")
            ->willReturn(fopen($patchToApply, "r"));

        $patcher->applyPatch($patch, $package, false, false);

        $this->assertEquals(file_get_contents($expectedOutputFile), file_get_contents($tempFile));
    }

    /**
     *
     * @throws Exception
     * @todo: Less copy paste
     * @covers \Visma\ComposerPatches\Util\Patcher::applyPatch
     */
    public function testThrowsErrorOnInvalidPatch()
    {
        $dir = Directory::tempdir();

        $fileToPatch = implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, 'Patcher', 'Fixtures', 'files', 'broken-class-1.php']
        );

        $patchToApply = implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, 'Patcher', 'Fixtures', 'patches', 'broken-patch-class-1.patch']
        );

        $fileName = basename($fileToPatch);

        $tempFile = implode(DIRECTORY_SEPARATOR, [$dir, $fileName]);

        // Copy broken file in
        copy($fileToPatch, $tempFile);

        /**
         * @var Composer|MockObject $composer
         * @var InstallationManager|MockObject $installationManager
         * @var Patch|MockObject $patch
         * @var PackageInterface|MockObject $package
         */
        $composer = $this->createMock(Composer::class);
        $patch = $this->createMock(Patch::class);
        $package = $this->createMock(PackageInterface::class);
        $installationManager = $this->createMock(InstallationManager::class);
        $installationManager->method("getInstallPath")->willReturn($dir);
        $composer->method("getInstallationManager")->willReturn($installationManager);

        $patcher = new \Visma\ComposerPatches\Util\Patcher(
            $composer,
            new Command(),
            $this->createMock(IOInterface::class)
        );

        $patch->method("getContents")
            ->willReturn(fopen($patchToApply, "r"));

        $this->expectException(PatchApplicationException::class);

        $patcher->applyPatch($patch, $package, false, false);
    }

    /**
     * @return void
     * @throws PatchApplicationException
     * @covers \Visma\ComposerPatches\Util\Patcher::applyPatch
     */
    public function testItAppliesPatches()
    {
        $composer = $this->createMock(Composer::class);
        $installationManager = $this->createMock(InstallationManager::class);
        $composer->method("getInstallationManager")->willReturn($installationManager);

        $patcher = new \Visma\ComposerPatches\Util\Patcher(
            $composer,
            new Command(),
            $this->createMock(IOInterface::class)
        );

        $patch = $this->createMock(Patch::class);
        $patch->method("getPackage")->willReturn("visma/package-advanced");
        $patch->method("getVersion")->willReturn("^1");
        $patchFile = implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, 'Patcher', 'Fixtures', 'patches', 'advanced-package-patch.patch']
        );
        $patchFile = implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, 'Patcher', 'Fixtures', 'patches', 'advanced-package-patch.patch']
        );
        $patch->method("getContents")->willReturn(fopen($patchFile, "r"));

        $patches = $this->createMock(PatchList::class);
        $patches->method("getPatches")->willReturn([$patch]);
        $packages = [];

        $packageToPatch = implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, 'Package', 'Fixtures', 'package_advanced_1']
        );

        $packageDir = implode(DIRECTORY_SEPARATOR, [Directory::tempdir(), "package"]);
        $installationManager->method("getInstallPath")
            ->willReturnCallback(function (PackageInterface $package) use ($packageDir) {
                switch ($package->getName()) {
                    case "visma/package-advanced":
                        return $packageDir;
                }
                return null;
            });

        Directory::rcopy($packageToPatch, $packageDir);

        $loader = new JsonLoader(new ArrayLoader());
        $package = $loader->load(implode(DIRECTORY_SEPARATOR, [$packageDir, "composer.json"]));

        $packages[] = $package;

        $patcher->applyPatches($patches, $packages, false);

        $expectedFilepath = implode(DIRECTORY_SEPARATOR, [$packageDir, "file_after_patch.php"]);
        $actualFilepath = implode(DIRECTORY_SEPARATOR, [$packageDir, "file_to_patch.php"]);

        $this->assertFileEquals($expectedFilepath, $actualFilepath);
    }

    /**
     * @return void
     * @throws PatchApplicationException
     * @covers \Visma\ComposerPatches\Util\Patcher::applyPatch
     */
    public function testItAppliesRelativePatches()
    {
        $tempDir = Directory::tempdir();
        chdir($tempDir);
        $packages = [];

        $projectFile = implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, 'Package', 'Fixtures', 'project-composer.json']
        );
        $packageToPatch = implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, 'Package', 'Fixtures', 'package_advanced_1']
        );
        $patchToApply = implode(
            DIRECTORY_SEPARATOR,
            [__DIR__, 'Package', 'Fixtures', 'package_advanced_with_patch_1']
        );

        $packageDir = implode(DIRECTORY_SEPARATOR, [$tempDir, "package"]);
        $patchDir = implode(DIRECTORY_SEPARATOR, [$tempDir, "patch"]);

        copy($projectFile, $tempDir . DIRECTORY_SEPARATOR . "composer.json");
        Directory::rcopy($packageToPatch, $packageDir);
        Directory::rcopy($patchToApply, $patchDir);

        $io = $this->createMock(IOInterface::class);
        $composer = $this->createMock(Composer::class);
        $im = $this->createMock(InstallationManager::class);
        $composer->method("getInstallationManager")->willReturn($im);
        $im->method("getInstallPath")->willReturnCallback(function ($package) use ($packageDir, $patchDir) {
            switch ($package->getName()) {
                case "visma/package-advanced":
                    return $packageDir;
                case "visma/package-advanced-patches":
                    return $patchDir;
                default:
                    return '';
            }
        });

        $patcher = new \Visma\ComposerPatches\Util\Patcher(
            $composer,
            new Command(),
            $this->createMock(IOInterface::class)
        );

        $loader = new JsonLoader(new ArrayLoader());
        $packages[] = $loader->load(implode(DIRECTORY_SEPARATOR, [$packageDir, "composer.json"]));
        $packages[] = $loader->load(implode(DIRECTORY_SEPARATOR, [$patchDir, "composer.json"]));

        $patchList = new PatchList(new RemoteFilesystem($io, new Config()), $io, new Psr16Cache(new ArrayAdapter()));
        $patchList->addPackages($packages);

        $patcher->applyPatches($patchList, $packages, false);

        $expectedFilepath = implode(DIRECTORY_SEPARATOR, [$packageDir, "file_after_patch.php"]);
        $actualFilepath = implode(DIRECTORY_SEPARATOR, [$packageDir, "file_to_patch.php"]);

        $this->assertFileEquals($expectedFilepath, $actualFilepath);
    }
}
