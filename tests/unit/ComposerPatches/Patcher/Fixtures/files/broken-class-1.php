<?php

/**
 * Class BrokenClassOne
 */
class BrokenClassOne
{
    /**
     * Some name
     *
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $name
     * @return BrokenClassOne
     */
    public function setName(string $name): BrokenClassOne
    {
        $this->name = $name;
    }
}
