# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.2.0] - 2025-01-17
### Changed
- Version constraints of Composer dependencies.
- Bitbucket pipelines.

[3.2.0]: https://bitbucket.org/vismadc/visma-composer-patches-plugin/branches/compare/3.2.0%0D3.1.1

## [3.1.1] - 2024-02-26
### Changed
- Increased plugin priority to make it possible to apply patches on Magento lib/web files.

[3.1.1]: https://bitbucket.org/vismadc/visma-composer-patches-plugin/branches/compare/3.1.1%0D3.1.0

## [3.1.0] - 2023-09-28
### Changed
- Update BB pipeline to v1.3.4
### Fixed
- PHPCS fixes
- PHPMD fixes
- Psalm fixes
- Tests
- Infection errors

[3.1.0]: https://bitbucket.org/vismadc/visma-composer-patches-plugin/branches/compare/3.1.0%0D3.0.4


## [3.0.4] - 2022-04-26
### Changed
- Update php version requirement

[3.0.4]: https://bitbucket.org/vismadc/visma-composer-patches-plugin/branches/compare/3.0.4%0D3.0.3


## [3.0.3] - 2022-04-26
### Changed
- Revert php8 composer support, needs more changes

[3.0.3]: https://bitbucket.org/vismadc/visma-composer-patches-plugin/branches/compare/3.0.3%0D3.0.2

## [3.0.2] - 2022-04-22
### Fixed
- symfony/cache version fix

[3.0.2]: https://bitbucket.org/vismadc/visma-composer-patches-plugin/branches/compare/3.0.2%0D3.0.1


## [3.0.1] -2022-04-22
### Fixed
* Composer require php8 support fix

[3.0.1]: https://bitbucket.org/vismadc/visma-composer-patches-plugin/branches/compare/3.0.1%0D3.0.0
[3.0.0]: https://bitbucket.org/vismadc/visma-composer-patches-plugin/src/3.0.0/
